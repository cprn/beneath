import { log } from "./logger";

export const scaleCanvas = (canvas, context) => {
    const scaleFactor = Math.round(window.devicePixelRatio);
    const scaledWidth = canvas.width * scaleFactor;
    const scaledHeight = canvas.height * scaleFactor;

    if (canvas.width !== scaledWidth || canvas.height !== scaledHeight) {
        canvas.width = scaledWidth;
        canvas.height = scaledHeight;
        log.info('Canvas scaled.');
        log.debug(`Canvas scale factor: ${scaleFactor}`);
    }

    context.scale(scaleFactor, scaleFactor);
};
