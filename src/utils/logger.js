export const LogLevel = {
    ERROR:   0,
    WARNING: 1,
    INFO:    2,
    DEBUG:   3,
};

export class Logger {
    constructor(level = LogLevel.INFO) {
        this.level = level ? level : LogLevel.INFO;
    }

    setLogLevel(level) {
        this.level = level;
    }

    debug(message) {
        this.log(message, LogLevel.DEBUG);
    }

    info(message) {
        this.log(message, LogLevel.INFO);
    }

    warning(message) {
        this.log(message, LogLevel.WARNING);
    }

    error(message) {
        this.log(message, LogLevel.ERROR);
    }

    log(message, level) {
        const label = Object.values(LogLevel).includes(level)
            ? Object.keys(LogLevel).find(k => LogLevel[k] === level)
            : 'UNKNOWN';

        if (level <= this.level) {
            console.log(`[${label}] ${message}`);
        }
    }
}

export const log = new Logger;
