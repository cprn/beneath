export const initializeInput = () => {
    const keys = {};

    const handleKeyDown = (event) => {
        keys[event.key] = true;
    };

    const handleKeyUp = (event) => {
        keys[event.key] = false;
    };

    document.addEventListener('keydown', handleKeyDown);
    document.addEventListener('keyup', handleKeyUp);

    return keys;
};

export const initializeMouseInput = (canvas) => {
    const mouse = {
        x: 0,
        y: 0,
    };

    const handleMouseMove = (event) => {
        const rect = canvas.getBoundingClientRect();
        mouse.x = event.clientX - rect.left;
        mouse.y = event.clientY - rect.top;
    };

    canvas.addEventListener('mousemove', handleMouseMove);

    return mouse;
};
