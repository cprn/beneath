import { startGame } from './game';

// Initialize the game when the DOM content is loaded
document.addEventListener('DOMContentLoaded', () => {
    startGame();
});
