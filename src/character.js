export class Character {
    constructor (x, y, radius = 20, speed = 5, rotation = 0, color = 'blue') {
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.speed = speed;
        this.rotation = rotation;
        this.color = color;
    }

    update (keys, mouse) {
        if (keys.ArrowUp || keys.w) {
            this.y -= this.speed;
        }
        if (keys.ArrowDown || keys.s) {
            this.y += this.speed;
        }
        if (keys.ArrowLeft || keys.a) {
            this.x -= this.speed;
        }
        if (keys.ArrowRight || keys.d) {
            this.x += this.speed;
        }

        const deltaX = mouse.x - this.x;
        const deltaY = mouse.y - this.y;
        this.rotation = Math.atan2(deltaY, deltaX);
    }
};
