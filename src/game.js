import { initializeInput, initializeMouseInput } from './utils/input';
import { scaleCanvas } from './utils/scaling';
import { Character } from './character';
import { LogLevel, log } from './utils/logger';

export const startGame = () => {
    log.setLogLevel(LogLevel.DEBUG);

    const canvas = document.createElement('canvas');
    canvas.id = 'gameCanvas';
    document.body.appendChild(canvas);
    const context = canvas.getContext('2d');
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    log.info('Canvas initialized.');

    const keys = initializeInput();
    const mouse = initializeMouseInput(canvas);
    log.info('Input initialized.');

    const character = new Character(canvas.width / 2, canvas.height / 2);

    const gameLoop = () => {
        scaleCanvas(canvas, context);

        context.clearRect(0, 0, canvas.width, canvas.height);
        log.info('Canvas cleared.');

        character.update(keys, mouse);

        context.save();
        context.translate(character.x, character.y);
        context.rotate(character.rotation);
        context.fillStyle = character.color;
        context.beginPath();
        context.arc(0, 0, character.radius, 0, Math.PI * 2);
        context.closePath();
        context.fill();
        context.restore();
        log.info('Character drawn.');

        requestAnimationFrame(gameLoop);
    };

    gameLoop();
    log.info('Game loop started.');
};
